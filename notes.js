/*Syntax: npx create-react-app project-name*/

//cd project-name
//After creating project execute the command 'npm start' to run our project in our localhost.

//Delete the unnecessary files from the newly created project
	// 1. App.test.js
	// 2. index.css
	// 3. logo.svg
	// reportWebVitals.js

//After deleting the unnecessary files, we encountered errors
	// errors encountered: importation of the deleted files
	// to fix we remove the syntax or codes of the imported files.

//Now we have a blank state where we can start building our own ReactJS app.

/*
	Create components Folder:
		ctrl + shift + p
		Search package Control Install Package and Babel


*/

//Creating Page:
	//First Create a new JS File
	//Create a Function na Exportable
	//import crc rule. (Bootstrap)
	//Mount our Highlights to Home.js



//Storing information in a context object is done by providing the information using the corresponding "Provider" component and passing the information via the "value" prop

//All information provided to the Provider component can be acecessed later on from the context object as properties