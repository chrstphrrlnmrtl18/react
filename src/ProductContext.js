import React from 'react';


//Create a context object
//A Context object as the name states is a data type that can be used to store information that can be shared to other components within the app
//The context object is a different approach to passing information between components and allows easier access by the use of prop-drilling

//CreateContext is use to create a context in react
const ProductContext = React.createContext();


//The "Provider" component allows other component to consume/use the context object and supply the necessary information needed to the context Object.
export const ProductProvider = ProducContext.Provider;

export default ProductContext;