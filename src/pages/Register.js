import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import {Container, Row, Col} from 'react-bootstrap';

//we use this to get the input of the user
import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';


export default function Register(){

//state hooks to store the values of the input field from our user
const [email, setEmail] = useState ('');
const [password1, setPassword1] = useState ('');
const [password2, setPassword2] = useState ('');

const [isActive, setIsActive] = useState(false);

const{user,setUser} = useContext(UserContext);

/*Business Logic*/
	// we want to disable the register button if one of the input fields is empty

useEffect(() => {
	/*console.log(email);
	console.log(password1);
	console.log(password2)*/

	if(email !== "" && password1 !== "" && password2 !== "" && password1 === password2){	
		setIsActive(true);
	}else{
		setIsActive(false);
	}
}, [email, password1, password2]);

// this function will be triggered when the inputs in the Form will be submitted

function registerUser(event){
	event.preventDefault()

	alert(`Congratulation ${email} you are now registered in our website!`)

	localStorage.setItem('email', email);
	setUser(localStorage.getItem("email"));


	setEmail('');
	setPassword1('');
	setPassword2('');

	
}


	return (
		(user.id !== null)?
		<Navigate to = '/' />
		:
		<Container>
		<Row>
			<Col className = "col-md-4 col-8 offset-md-4 offset-2">
				<Form onSubmit = {registerUser} className = 'bg-secondary p-3'>

				  <Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control 
				    	type="email" 
				    	placeholder="Enter email" 
				    	value = {email}
				    	onChange = {event => setEmail(event.target.value)}
				    	required/>
				    <Form.Text className="text-muted">
				      We'll never share your email with anyone else.
				    </Form.Text>
				  </Form.Group>


				  <Form.Group className="mb-3" controlId="password1" >
				    <Form.Label>Enter your desired Password</Form.Label>
				    <Form.Control 
				    	type="password" 
				    	placeholder="Password" 
				    	value = {password1}
				    	onChange = {event => setPassword1(event.target.value)}
				    	required/>
				  </Form.Group>


				<Form.Group className="mb-3" controlId="password2">
				    <Form.Label>Enter your desired Password</Form.Label>
				    <Form.Control 
				    	type="password" 
				    	placeholder="Password" 
				    	value = {password2}
				    	onChange = {event => setPassword2(event.target.value)}
				    	required/>
				 </Form.Group>
				  
				  <Button variant="primary" type="submit" disabled= {!isActive}>
				    Register
				  </Button>
				</Form>
			</Col>

		</Row>
	    </Container>
	)
}