//import of the classes needed fro the CRC Rule as well as 
import {Row, Col,Card} from 'react-bootstrap';


export default function Highlights(){

	return(
		<Row className = "mt-3 mb-3">

			{/*This is the First Card*/}
			<Col xs={12} md={4}>
				<Card className = "cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>
				        	<h2>Learn From Home</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ut bibendum purus, quis rutrum metus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas ac nisi lectus. Vivamus sit amet nibh lorem. Proin porta sagittis ipsum vitae pellentesque. Nam posuere rhoncus augue. Nam a urna ultrices, aliquet enim non, ullamcorper mauris. Phasellus pretium erat vitae eros vehicula fermentum. Praesent pretium purus libero, id laoreet lectus vulputate non. Proin iaculis feugiat urna quis lobortis.
				        </Card.Text>
				    </Card.Body>
				</Card>
			</Col>

			{/*This is The Second Card*/}
			<Col xs={12} md={4}>
				<Card className = "cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>
				        	<h2>Study Now, Pay Later</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ut bibendum purus, quis rutrum metus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas ac nisi lectus. Vivamus sit amet nibh lorem. Proin porta sagittis ipsum vitae pellentesque. Nam posuere rhoncus augue. Nam a urna ultrices, aliquet enim non, ullamcorper mauris. Phasellus pretium erat vitae eros vehicula fermentum. Praesent pretium purus libero, id laoreet lectus vulputate non. Proin iaculis feugiat urna quis lobortis.
				        </Card.Text>
				    </Card.Body>
				</Card>
			</Col>


			{/*This is The Third Card*/}
			<Col xs={12} md={4}>
				<Card className = "cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>
				        	<h2>Be Part of our Community</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ut bibendum purus, quis rutrum metus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas ac nisi lectus. Vivamus sit amet nibh lorem. Proin porta sagittis ipsum vitae pellentesque. Nam posuere rhoncus augue. Nam a urna ultrices, aliquet enim non, ullamcorper mauris. Phasellus pretium erat vitae eros vehicula fermentum. Praesent pretium purus libero, id laoreet lectus vulputate non. Proin iaculis feugiat urna quis lobortis.
				        </Card.Text>
				    </Card.Body>
				</Card>
			</Col>


		</Row>

		)
}